import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { DayList } from './components/DayList';

ReactDOM.render(
  <React.StrictMode>
    <DayList />
  </React.StrictMode>,
  document.getElementById('root'),
);
