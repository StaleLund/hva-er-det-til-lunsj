import { Weekday } from './weekday';

export type Day = {
  day: Weekday,
  dishes: string[],
}

export type ApiData = {
  weekNumber: number,
  days: Day[],
}
