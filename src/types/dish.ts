export type Dish = {
  type: string,
  dish: string,
  favourite: boolean,
};
